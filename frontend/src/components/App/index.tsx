import React, { FC } from 'react'
import { ApolloProvider } from '@apollo/client'
import { createMuiTheme } from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'
import Container from '@material-ui/core/Container'
import ProductDropper from '../ProductDropper'
import client from './apollo-client'

const darkTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      light: '#a6d4fa',
      main: '#90caf9',
      dark: '#648dae',
    },
    secondary: {
      light: '#f6a5c0',
      main: '#f48fb1',
      dark: '#aa647b',
    },
    error: {
      light: '#e57373',
      main: '#f44336',
      dark: '#d32f2f',
    },
  },
})

const App: FC = () => (
  <ApolloProvider client={client}>
    <ThemeProvider theme={darkTheme}>
      <Container maxWidth="lg">
        <ProductDropper />
      </Container>
    </ThemeProvider>
  </ApolloProvider>
)

export default App
