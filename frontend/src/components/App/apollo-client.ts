import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client'
import { GRAPHQL_URL } from '../../config/constants'

const cache = new InMemoryCache()

export default new ApolloClient({
  cache,
  link: new HttpLink({
    uri: GRAPHQL_URL,
  }),
})
