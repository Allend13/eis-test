import React, { FC, useMemo } from 'react'
import Grid from '@material-ui/core/Grid'
import ProductDropperListItem from './ProductDropperListItem'
import { ProductDropperListProps } from './interfaces'
import styles from './styles.module.css'

const ProductDropperList: FC<ProductDropperListProps> = ({
  products,
  removeProduct,
}) => {
  const productsList = useMemo(
    () =>
      products.map((product) => (
        <ProductDropperListItem
          key={product._id}
          product={product}
          removeProduct={removeProduct}
        />
      )),
    [products],
  )
  return (
    <div className={styles.root}>
      <Grid container justify="center" spacing={3}>
        {productsList}
      </Grid>
    </div>
  )
}

export default ProductDropperList
