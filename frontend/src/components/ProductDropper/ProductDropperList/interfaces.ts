import { ProductListItemFragment } from '../../../generated/graphql'

export interface ProductDropperListProps {
  /** Products list */
  products: ProductListItemFragment[]
  /** Remove product by id function */
  removeProduct: (id: string) => void
}
