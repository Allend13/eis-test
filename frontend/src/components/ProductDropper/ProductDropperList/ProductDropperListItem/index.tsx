import React, { FC, useCallback } from 'react'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import IconButton from '@material-ui/core/IconButton'
import DeleteOutline from '@material-ui/icons/DeleteOutline'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import { ProductDropperListItemProps } from './interfaces'
import styles from './styles.module.css'

const ProductDropperListItem: FC<ProductDropperListItemProps> = ({
  product,
  removeProduct,
}) => {
  const handleIconClick = useCallback(() => {
    removeProduct(product._id)
  }, [removeProduct, product])

  const action = (
    <IconButton aria-label="remove" onClick={handleIconClick}>
      <DeleteOutline />
    </IconButton>
  )

  const { name, image, price, quantity, color } = product

  return (
    <Grid item xs={12} sm={6} md={4} lg={3}>
      <Card>
        <CardHeader title={name} action={action} />

        <CardMedia image={image} title={name} className={styles.media} />

        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p">
            <strong>Price:</strong> {price}$
          </Typography>
          <Typography
            variant="body2"
            component="p"
            color={quantity ? 'textSecondary' : 'error'}
          >
            {quantity ? (
              <span>
                <strong>Quantity:</strong> {quantity}{' '}
              </span>
            ) : (
              'Out of order'
            )}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            <strong>Color:</strong> {product.color}
          </Typography>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default ProductDropperListItem
