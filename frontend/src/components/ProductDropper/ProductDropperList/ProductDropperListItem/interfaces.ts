import { ProductListItemFragment } from '../../../../generated/graphql'

export interface ProductDropperListItemProps {
  /** Products list */
  product: ProductListItemFragment
  /** Remove product by id function */
  removeProduct: (id: string) => void
}
