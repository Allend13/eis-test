import React, { FC, useCallback, useMemo } from 'react'
import debounce from 'lodash/debounce'
import CircularProgress from '@material-ui/core/CircularProgress'
import {
  useProductsListQuery,
  useSearchProductsLazyQuery,
  useRemoveProductByIdMutation,
} from '../../generated/graphql'
import styles from './styles.module.css'

import ProductDropperSearch from './ProductDropperSearch'
import ProductDropperDropzone from './ProductDropperDropzone'
import ProductDropperList from './ProductDropperList'

const ProductDropper: FC = () => {
  const {
    data: initData,
    loading: initLoading,
    refetch: listRefetch,
  } = useProductsListQuery({
    fetchPolicy: 'cache-and-network',
  })
  const [
    searchProducts,
    {
      data: searchData,
      loading: searchLoading,
      called: searchCalled,
      refetch: searchRefetch,
    },
  ] = useSearchProductsLazyQuery({
    fetchPolicy: 'cache-and-network',
  })

  const refetch = useCallback(() => {
    if (searchCalled) {
      searchRefetch && searchRefetch()
    } else {
      listRefetch()
    }
  }, [searchCalled, searchRefetch, listRefetch])

  const [removeProductByIdMutation] = useRemoveProductByIdMutation({
    onCompleted: refetch,
  })

  const removeProduct = useCallback(
    (id: string) => removeProductByIdMutation({ variables: { id } }),
    [removeProductByIdMutation],
  )

  const loading = initLoading || searchLoading

  const debouncedSearch = useMemo(() => debounce(searchProducts, 300), [
    searchProducts,
  ])

  const content = useMemo(() => {
    if (loading) return <CircularProgress />

    const data = searchCalled ? searchData?.searchProducts : initData?.products
    const products = data || []

    return (
      <ProductDropperList products={products} removeProduct={removeProduct} />
    )
  }, [initData, searchData, loading, removeProduct])

  return (
    <div className={styles.root}>
      <ProductDropperSearch search={debouncedSearch} />
      <ProductDropperDropzone refetch={refetch} />
      {content}
    </div>
  )
}

export default ProductDropper
