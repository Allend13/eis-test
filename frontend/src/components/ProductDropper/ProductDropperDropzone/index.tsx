import React, { FC, useCallback, useState } from 'react'
import { useDropzone } from 'react-dropzone'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import {
  useAddProductMutation,
  AddProductInput,
} from '../../../generated/graphql'
import ProductDropperDropzoneModal from './ProductDropperDropzoneModal'
import useFileUpload from './hooks/useFileUpload'
import { ProductDropperDropzoneProps } from './interfaces'
import styles from './styles.module.css'

const ProductDropperDropzone: FC<ProductDropperDropzoneProps> = ({
  refetch,
}) => {
  const { uploadFile, loading: imgLoading, url: imgUrl } = useFileUpload()
  const [modalOpen, toggleModal] = useState(false)
  const [addProduct, { loading: addProductLoading }] = useAddProductMutation({
    onCompleted: refetch,
  })

  const openModal = useCallback(() => toggleModal(true), [toggleModal])
  const closeModal = useCallback(() => toggleModal(false), [toggleModal])

  const addProductCallback = useCallback(
    async (productInput: AddProductInput) => {
      await addProduct({ variables: { productInput } })
      closeModal()
    },
    [addProduct],
  )

  const onDrop = useCallback(
    (acceptedFiles: File[]) => {
      const file = acceptedFiles[0]

      uploadFile(file)
      openModal()
    },
    [uploadFile, openModal],
  )

  const { getRootProps, getInputProps } = useDropzone({ onDrop })

  return (
    <>
      <div className={styles.root}>
        <input
          {...getInputProps()}
          hidden
          accept="image/x-png,image/gif,image/jpeg"
        />

        <Grid container justify="space-between" spacing={3}>
          <Grid item>
            <div {...getRootProps()}>
              <Button variant="outlined" color="primary" size="large">
                Add product
              </Button>
            </div>
          </Grid>

          <Grid item xs>
            <Paper className={styles.paper} {...getRootProps()}>
              <Typography variant="caption">
                or drop your product image here
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      </div>

      {modalOpen && (
        <ProductDropperDropzoneModal
          open={modalOpen}
          handleClose={closeModal}
          handleSubmit={addProductCallback}
          submitLoading={addProductLoading}
          image={imgUrl}
          imageLoading={imgLoading}
        />
      )}
    </>
  )
}

export default ProductDropperDropzone
