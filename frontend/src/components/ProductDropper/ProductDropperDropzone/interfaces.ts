export interface ProductDropperDropzoneProps {
  /* Refetch list on product add */
  refetch: () => void
}
