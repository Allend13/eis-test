import { useCallback, useReducer } from 'react'
import { FILE_UPLOAD_URL } from '../../../../config/constants'

type UseFileUploadResult = {
  /** Upload file function */
  uploadFile: (file: File) => void
} & UseFileUploadState

type UseFileUploadState = {
  /** File url on success */
  url: string | null
  /** File uploading status */
  loading: boolean
  /** File uploading failed */
  error: boolean
}

enum UseFileUploadActionTypes {
  START = 'UPLOAD_STARTED',
  SUCCESS = 'UPLOAD_SUCCESS',
  FAIL = 'UPLOAD_FAILED',
}

type UseFileUploadNoPayloadAction = {
  type: UseFileUploadActionTypes.START | UseFileUploadActionTypes.FAIL
}

type UseFileUploadPayloadAction = {
  type: UseFileUploadActionTypes.SUCCESS
  payload: string
}

type UseFileUploadAction =
  | UseFileUploadNoPayloadAction
  | UseFileUploadPayloadAction

function reducer(
  state: UseFileUploadState,
  action: UseFileUploadAction,
): UseFileUploadState {
  switch (action.type) {
    case UseFileUploadActionTypes.START:
      return { loading: true, error: false, url: null }
    case UseFileUploadActionTypes.SUCCESS:
      return { loading: false, error: false, url: action.payload }
    case UseFileUploadActionTypes.FAIL:
      return { loading: false, error: true, url: null }
    default:
      return state
  }
}

const initialState = { loading: false, error: false, url: null }

export default function useFileUpload(): UseFileUploadResult {
  const [state, dispatch] = useReducer(reducer, initialState)

  const uploadFile = useCallback(
    (file: File) => {
      dispatch({ type: UseFileUploadActionTypes.START })

      const form = new FormData()
      form.append('productImage', file)

      fetch(FILE_UPLOAD_URL, {
        method: 'POST',
        body: form,
      })
        .then(async (res) => {
          const result = (await res.json()) as { url: string }

          dispatch({
            type: UseFileUploadActionTypes.SUCCESS,
            payload: result.url,
          })
        })
        .catch(() => dispatch({ type: UseFileUploadActionTypes.FAIL }))
    },
    [dispatch],
  )

  return { ...state, uploadFile }
}
