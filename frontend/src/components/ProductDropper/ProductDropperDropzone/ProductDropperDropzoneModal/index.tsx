import React, { FC, useCallback, useEffect } from 'react'
import { useForm, Controller } from 'react-hook-form'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Modal from '@material-ui/core/Modal'
import Paper from '@material-ui/core/Paper'
import Backdrop from '@material-ui/core/Backdrop'
import Fade from '@material-ui/core/Fade'
import Grid from '@material-ui/core/Grid'
import CircularProgress from '@material-ui/core/CircularProgress'
import CardMedia from '@material-ui/core/CardMedia'
import * as yup from 'yup'
import { SERVER } from '../../../../config/constants'
import { ProductDropperDropzoneModalProps, FormData } from './interfaces'
import styles from './styles.module.css'

const REQUIRED_MESSAGE = 'is required'
const TYPE_ERROR_MESSAGE = 'must be a number'

const ProductFormSchema = yup.object().shape({
  name: yup.string().required(REQUIRED_MESSAGE),
  quantity: yup
    .number()
    .typeError(TYPE_ERROR_MESSAGE)
    .required(REQUIRED_MESSAGE),
  price: yup.number().typeError(TYPE_ERROR_MESSAGE).required(REQUIRED_MESSAGE),
  color: yup.string().required(REQUIRED_MESSAGE),
  image: yup.string().required(REQUIRED_MESSAGE),
})

const ProductDropperDropzoneModal: FC<ProductDropperDropzoneModalProps> = ({
  open,
  handleClose,
  image,
  imageLoading,
  handleSubmit,
  submitLoading,
}) => {
  const {
    control,
    handleSubmit: handleFormSubmit,
    register,
    setValue,
    errors,
    getValues,
  } = useForm<FormData>({
    validationSchema: ProductFormSchema,
  })

  useEffect(() => register({ name: 'image' }), [register])

  const imageAbsoluteUrl = `${SERVER}${image}`

  useEffect(() => {
    if (image) setValue('image', `${SERVER}${image}`)
  }, [image, setValue])

  const onSubmit = useCallback(() => {
    const values = getValues()
    const productInput = {
      ...values,
      quantity: parseInt(values.quantity, 10),
      price: parseInt(values.price, 10),
    }

    handleSubmit(productInput)
  }, [getValues, handleSubmit])

  return (
    <Modal
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
      className={styles.root}
    >
      <Fade in={open}>
        <Paper className={styles.paper}>
          <div className={styles.mediaContainer}>
            <div className={styles.mediaInner}>
              {imageLoading && (
                <CircularProgress className={styles.imgProgress} />
              )}
              {!imageLoading && image && (
                <CardMedia
                  image={imageAbsoluteUrl}
                  className={styles.mediaImage}
                />
              )}
            </div>
          </div>

          <form onSubmit={handleFormSubmit(onSubmit)} className={styles.form}>
            <div className={styles.inputArea}>
              <Controller
                as={TextField}
                name="name"
                control={control}
                defaultValue="Product XYZ"
                label="Product name"
                error={!!errors.name}
                helperText={errors.name?.message}
              />
            </div>
            <div className={styles.inputArea}>
              <Controller
                as={TextField}
                name="quantity"
                type="number"
                defaultValue={1}
                control={control}
                label="Quantity"
                error={!!errors.quantity}
                helperText={errors.quantity?.message}
              />
            </div>
            <div className={styles.inputArea}>
              <Controller
                as={TextField}
                name="price"
                type="number"
                control={control}
                defaultValue={1}
                label="Price $"
                error={!!errors.price}
                helperText={errors.price?.message}
              />
            </div>
            <div className={styles.inputArea}>
              <Controller
                as={TextField}
                name="color"
                control={control}
                defaultValue="white"
                label="Color"
                error={!!errors.color}
                helperText={errors.color?.message}
              />
            </div>
          </form>

          <div className={styles.footer}>
            <Grid container justify="space-between">
              <Grid item>
                <Button
                  variant="outlined"
                  color="primary"
                  size="large"
                  role="submit"
                  onClick={handleFormSubmit(onSubmit)}
                  disabled={submitLoading}
                >
                  {submitLoading ? (
                    <CircularProgress className={styles.imgProgress} />
                  ) : (
                    'Add product'
                  )}
                </Button>
              </Grid>
              <Grid item>
                <Button
                  variant="outlined"
                  color="secondary"
                  size="large"
                  onClick={handleClose}
                  disabled={submitLoading}
                >
                  Cancel
                </Button>
              </Grid>
            </Grid>
          </div>
        </Paper>
      </Fade>
    </Modal>
  )
}

export default ProductDropperDropzoneModal
