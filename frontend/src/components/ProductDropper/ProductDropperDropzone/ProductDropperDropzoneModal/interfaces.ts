import { AddProductInput } from '../../../../generated/graphql'

export type FormData = {
  /** product name */
  name: string
  /** product quantity */
  quantity: string
  /** product price */
  price: string
  /** product color */
  color: string
  /** product image url */
  image: string
}

export interface ProductDropperDropzoneModalProps {
  /* Is modal open */
  open: boolean
  /* Close modal function */
  handleClose: () => void
  /* Product img url */
  image: string | null
  /* Product image loading status */
  imageLoading: boolean
  /* Submit form data */
  handleSubmit: (productInput: AddProductInput) => void
  /* Submit form loading status */
  submitLoading: boolean
}
