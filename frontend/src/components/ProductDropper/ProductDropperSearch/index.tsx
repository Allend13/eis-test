import React, { FC, useCallback } from 'react'
import TextField from '@material-ui/core/TextField'
import { ProductDropperSearchProps } from './interfaces'
import styles from './styles.module.css'

const ProductDropperSearch: FC<ProductDropperSearchProps> = ({ search }) => {
  const handleChange = useCallback(
    (evt: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = evt.currentTarget

      search({ variables: { filter: { name: value } } })
    },
    [search],
  )
  return (
    <TextField
      id="standard-basic"
      label="Search products by name"
      onChange={handleChange}
      className={styles.root}
    />
  )
}

export default ProductDropperSearch
