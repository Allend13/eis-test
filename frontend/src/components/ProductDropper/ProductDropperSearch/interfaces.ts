import { SearchProducts } from '../interfaces'

export interface ProductDropperSearchProps {
  /** Search products gql query function */
  search: SearchProducts
}
