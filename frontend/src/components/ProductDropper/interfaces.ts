import { QueryLazyOptions } from '@apollo/client'
import { SearchProductsQueryVariables } from '../../generated/graphql'

export type SearchProducts = (
  /** Search products gql query function */
  options?: QueryLazyOptions<SearchProductsQueryVariables> | undefined,
) => void
