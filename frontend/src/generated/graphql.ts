/* eslint-disable */
import gql from 'graphql-tag'
import * as ApolloReactCommon from '@apollo/client'
import * as ApolloReactHooks from '@apollo/client'
export type Maybe<T> = T | null

/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string
  String: string
  Boolean: boolean
  Int: number
  Float: number
  URL: any
}

export type Query = {
  __typename: 'Query'
  /** product by id */
  product?: Maybe<Product>
  /** list of products */
  products: Array<Product>
  /** search products */
  searchProducts: Array<Product>
}

export type QueryProductArgs = {
  id: Scalars['ID']
}

export type QueryProductsArgs = {
  skip?: Maybe<Scalars['Int']>
  take?: Maybe<Scalars['Int']>
}

export type QuerySearchProductsArgs = {
  skip?: Maybe<Scalars['Int']>
  take?: Maybe<Scalars['Int']>
  filter: SearchProductInput
}

export type Mutation = {
  __typename: 'Mutation'
  /** add product item */
  addProduct: Product
  /** remove product item by id */
  removeProductById: Scalars['ID']
}

export type MutationAddProductArgs = {
  product: AddProductInput
}

export type MutationRemoveProductByIdArgs = {
  id: Scalars['ID']
}

export type AddProductInput = {
  /** product name */
  name: Scalars['String']
  /** product quantity */
  quantity: Scalars['Int']
  /** product price */
  price: Scalars['Float']
  /** product color */
  color: Scalars['String']
  /** product image url */
  image: Scalars['URL']
}

/** Product item */
export type Product = {
  __typename: 'Product'
  /** product id */
  _id: Scalars['ID']
  /** product name */
  name: Scalars['String']
  /** product quantity */
  quantity: Scalars['Int']
  /** product price */
  price: Scalars['Float']
  /** product color */
  color: Scalars['String']
  /** product image url */
  image: Scalars['URL']
}

/** Input для поиска */
export type SearchProductInput = {
  /** search be name */
  name: Scalars['String']
}

export type ProductsListQueryVariables = {}

export type ProductsListQuery = {
  __typename: 'Query'
  products: Array<{ __typename: 'Product' } & ProductListItemFragment>
}

export type SearchProductsQueryVariables = {
  filter: SearchProductInput
}

export type SearchProductsQuery = {
  __typename: 'Query'
  searchProducts: Array<{ __typename: 'Product' } & ProductListItemFragment>
}

export type AddProductMutationVariables = {
  productInput: AddProductInput
}

export type AddProductMutation = {
  __typename: 'Mutation'
  addProduct: { __typename: 'Product' } & ProductListItemFragment
}

export type RemoveProductByIdMutationVariables = {
  id: Scalars['ID']
}

export type RemoveProductByIdMutation = {
  __typename: 'Mutation'
  removeProductById: string
}

export type ProductListItemFragment = {
  __typename: 'Product'
  _id: string
  name: string
  quantity: number
  price: number
  color: string
  image: any
}

export const ProductListItemFragmentDoc = gql`
  fragment productListItem on Product {
    _id
    name
    quantity
    price
    color
    image
  }
`
export const ProductsListDocument = gql`
  query productsList {
    products {
      ...productListItem
    }
  }
  ${ProductListItemFragmentDoc}
`

/**
 * __useProductsListQuery__
 *
 * To run a query within a React component, call `useProductsListQuery` and pass it any options that fit your needs.
 * When your component renders, `useProductsListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useProductsListQuery({
 *   variables: {
 *   },
 * });
 */
export function useProductsListQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    ProductsListQuery,
    ProductsListQueryVariables
  >,
) {
  return ApolloReactHooks.useQuery<
    ProductsListQuery,
    ProductsListQueryVariables
  >(ProductsListDocument, baseOptions)
}
export function useProductsListLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    ProductsListQuery,
    ProductsListQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<
    ProductsListQuery,
    ProductsListQueryVariables
  >(ProductsListDocument, baseOptions)
}
export type ProductsListQueryHookResult = ReturnType<
  typeof useProductsListQuery
>
export type ProductsListLazyQueryHookResult = ReturnType<
  typeof useProductsListLazyQuery
>
export type ProductsListQueryResult = ApolloReactCommon.QueryResult<
  ProductsListQuery,
  ProductsListQueryVariables
>
export const SearchProductsDocument = gql`
  query searchProducts($filter: SearchProductInput!) {
    searchProducts(filter: $filter) {
      ...productListItem
    }
  }
  ${ProductListItemFragmentDoc}
`

/**
 * __useSearchProductsQuery__
 *
 * To run a query within a React component, call `useSearchProductsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchProductsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchProductsQuery({
 *   variables: {
 *      filter: // value for 'filter'
 *   },
 * });
 */
export function useSearchProductsQuery(
  baseOptions?: ApolloReactHooks.QueryHookOptions<
    SearchProductsQuery,
    SearchProductsQueryVariables
  >,
) {
  return ApolloReactHooks.useQuery<
    SearchProductsQuery,
    SearchProductsQueryVariables
  >(SearchProductsDocument, baseOptions)
}
export function useSearchProductsLazyQuery(
  baseOptions?: ApolloReactHooks.LazyQueryHookOptions<
    SearchProductsQuery,
    SearchProductsQueryVariables
  >,
) {
  return ApolloReactHooks.useLazyQuery<
    SearchProductsQuery,
    SearchProductsQueryVariables
  >(SearchProductsDocument, baseOptions)
}
export type SearchProductsQueryHookResult = ReturnType<
  typeof useSearchProductsQuery
>
export type SearchProductsLazyQueryHookResult = ReturnType<
  typeof useSearchProductsLazyQuery
>
export type SearchProductsQueryResult = ApolloReactCommon.QueryResult<
  SearchProductsQuery,
  SearchProductsQueryVariables
>
export const AddProductDocument = gql`
  mutation addProduct($productInput: addProductInput!) {
    addProduct(product: $productInput) {
      ...productListItem
    }
  }
  ${ProductListItemFragmentDoc}
`
export type AddProductMutationFn = ApolloReactCommon.MutationFunction<
  AddProductMutation,
  AddProductMutationVariables
>

/**
 * __useAddProductMutation__
 *
 * To run a mutation, you first call `useAddProductMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddProductMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addProductMutation, { data, loading, error }] = useAddProductMutation({
 *   variables: {
 *      productInput: // value for 'productInput'
 *   },
 * });
 */
export function useAddProductMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    AddProductMutation,
    AddProductMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<
    AddProductMutation,
    AddProductMutationVariables
  >(AddProductDocument, baseOptions)
}
export type AddProductMutationHookResult = ReturnType<
  typeof useAddProductMutation
>
export type AddProductMutationResult = ApolloReactCommon.MutationResult<
  AddProductMutation
>
export type AddProductMutationOptions = ApolloReactCommon.BaseMutationOptions<
  AddProductMutation,
  AddProductMutationVariables
>
export const RemoveProductByIdDocument = gql`
  mutation removeProductById($id: ID!) {
    removeProductById(id: $id)
  }
`
export type RemoveProductByIdMutationFn = ApolloReactCommon.MutationFunction<
  RemoveProductByIdMutation,
  RemoveProductByIdMutationVariables
>

/**
 * __useRemoveProductByIdMutation__
 *
 * To run a mutation, you first call `useRemoveProductByIdMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRemoveProductByIdMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [removeProductByIdMutation, { data, loading, error }] = useRemoveProductByIdMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useRemoveProductByIdMutation(
  baseOptions?: ApolloReactHooks.MutationHookOptions<
    RemoveProductByIdMutation,
    RemoveProductByIdMutationVariables
  >,
) {
  return ApolloReactHooks.useMutation<
    RemoveProductByIdMutation,
    RemoveProductByIdMutationVariables
  >(RemoveProductByIdDocument, baseOptions)
}
export type RemoveProductByIdMutationHookResult = ReturnType<
  typeof useRemoveProductByIdMutation
>
export type RemoveProductByIdMutationResult = ApolloReactCommon.MutationResult<
  RemoveProductByIdMutation
>
export type RemoveProductByIdMutationOptions = ApolloReactCommon.BaseMutationOptions<
  RemoveProductByIdMutation,
  RemoveProductByIdMutationVariables
>
