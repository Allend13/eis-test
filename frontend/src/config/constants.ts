const { NODE_ENV, REACT_APP_DEV_API, REACT_APP_STAGE_API } = process.env
const isProd = NODE_ENV === 'production'

export const SERVER = isProd ? REACT_APP_STAGE_API : REACT_APP_DEV_API
export const GRAPHQL_URL = `${SERVER}/graphql`
export const FILE_UPLOAD_URL = `${SERVER}/upload`
