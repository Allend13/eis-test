const { ObjectId } = require('mongodb')

const data = [
    {
        _id: ObjectId('5df614d9f952bc4da8107ad3'),
        name: 'Orange',
        quantity: 150,
        price: 5,
        color: 'orange',
        image: 'https://www.irishtimes.com/polopoly_fs/1.3923226.1560339148!/image/image.jpg_gen/derivatives/ratio_1x1_w1200/image.jpg'
    },
    {
        _id: ObjectId('5e424e3c22f4553006b198df'),
        name: 'Banana',
        quantity: 666,
        price: 3.5,
        color: 'yellow',
        image: 'https://av.ru/product/h7b/h0f/8884788199454.jpg'
    },
    {
        _id: ObjectId('5e4451309f4455aa8ff5ad7c'),
        name: 'Apple',
        quantity: 1230,
        price: 9.99,
        color: 'green',
        image: 'https://sc01.alicdn.com/kf/HTB1HHDUbinrK1RjSsziq6xptpXa2.jpg'
    },
    {
        _id: ObjectId('5e4831fce699ca40a9d85df9'),
        name: 'Blackberry',
        quantity: 560,
        price: 15.00,
        color: 'black',
        image: 'https://previews.123rf.com/images/ovydyborets/ovydyborets1502/ovydyborets150200020/36247467-blackberry-fruit-isolated.jpg'
    },
    {
        _id: ObjectId('5e4831ffe699ca40a9d85dfa'),
        name: 'Cherry',
        quantity: 350,
        price: 7.45,
        color: 'red',
        image: 'https://i.pinimg.com/originals/f1/2c/69/f12c698588e1927ff6241b6a8d98338e.jpg'
    },
]

module.exports = {
    async up(db) {
        await db.collection('products').insertMany(data)
    },
}
