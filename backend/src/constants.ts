// Connection URL
export const dbUrl = process.env.MONGODB_URI || 'mongodb://eis-admin:eis-admin@localhost:27017'

// Database Name
export const dbName = 'eis'
