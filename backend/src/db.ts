import { MongoClient, Db } from 'mongodb'
import { dbName, dbUrl } from './constants'

export default async function createClient(dbUrl: string): Promise<Db> {
    const mongoClient = await MongoClient.connect(dbUrl)
    return mongoClient.db(dbName)
}

export const DBclient = createClient(dbUrl)

export let db: Db

DBclient.then(connection => {
    db = connection as Db
}).catch(reason => {
    return reason
})
