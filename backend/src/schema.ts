import path from 'path'
import { makeExecutableSchema } from 'graphql-tools'
import { URLResolver } from 'graphql-scalars'
import { fileLoader, mergeTypes, mergeResolvers } from 'merge-graphql-schemas'

const typesArray = fileLoader(path.join(__dirname, './modules/**/*.graphql'))
const resolverArray = fileLoader(path.join(__dirname, './modules/**/resolver.*'))

const scalarResolvers = {
    URL: URLResolver,
}

const typeDefs = mergeTypes(typesArray)
const resolvers = mergeResolvers([scalarResolvers, ...resolverArray])

export default makeExecutableSchema({ typeDefs: [typeDefs], resolvers })
