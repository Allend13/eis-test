import express from 'express'
import 'reflect-metadata'
import { ApolloServer } from 'apollo-server-express'
import { createServer } from 'http'
import compression from 'compression'
import cors from 'cors'

import schema from './schema'
import { DBclient } from './db'
import { getMulterCb, fileUploaderCb } from './services/file-uploader'
import { Db, MongoError } from 'mongodb'
import { Context, Request } from './interface'

require('dotenv').config()

const port = process.env.PORT || 4000

async function bootstrap(): Promise<void> {
    let db: Db
    let dbConnectionError: MongoError | null = null
    const app = express()

    if (dbConnectionError) return Promise.reject(dbConnectionError)

    await DBclient.then((connection: Db) => {
        db = connection
    }).catch(reason => {
        dbConnectionError = reason
        return reason
    })

    if (dbConnectionError) return Promise.reject(dbConnectionError)

    const server = new ApolloServer({
        schema,
        context: ({ req }: { req: Request }): Context => ({
            req,
            db,
        }),
    })

    app.use('*', cors())
    app.use(compression())
    app.post('/upload', getMulterCb(), fileUploaderCb)
    app.use(express.static('public'))
    server.applyMiddleware({ app, path: '/graphql' })

    createServer(app)

    app.listen({ port }, (): void =>
        console.log(`\n🚀      GraphQL is now running on http://localhost:${port}/graphql`),
    )
}

bootstrap()
    .then(() => console.log('🙏🤔 To be or not to be? 🤔🙏'))
    .catch(reason => console.log('😱 >> ', reason))
