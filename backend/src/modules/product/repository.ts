import {  ObjectId } from 'mongodb'

import { db } from '../../db'
import { ProductDbObject, SearchProductInput, AddProductInput } from '../../types'
import { DB_COLLECTION } from '../interface'

const { PRODUCTS } = DB_COLLECTION

export function findAll({ skip, take }: { skip: number; take: number }): Promise<ProductDbObject[]> {
    const res = db
        .collection<ProductDbObject>(PRODUCTS)
        .aggregate([{ $sort: { _id: -1 } }, { $skip: skip }, { $limit: take }])

    return res.toArray()
}

export function findById(id: string | ObjectId): Promise<ProductDbObject | null> {
    return db.collection<ProductDbObject>(PRODUCTS).findOne({ _id: new ObjectId(id) })
}

export async function addProduct(productInput: AddProductInput): Promise<ProductDbObject> {
    const { insertedId } = await db.collection<ProductDbObject>(PRODUCTS).insertOne(productInput)
    return {
        _id: insertedId,
        ...productInput
    }
}

export async function removeProductById(id: string | ObjectId): Promise<string | ObjectId> {
    await db.collection<ProductDbObject>(PRODUCTS).deleteOne({ _id: new ObjectId(id) })
    return id
}

export function findByFilter(skip: number, take: number, filter: SearchProductInput): Promise<ProductDbObject[]> {
    const res = db
        .collection<ProductDbObject>(PRODUCTS)
        .find({ name: new RegExp(filter.name, 'i') } )
        .skip(skip)
        .limit(take)

    return res.toArray()
}
