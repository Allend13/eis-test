import { Resolvers } from '../../types'

import { findAll, findById, addProduct, removeProductById } from './repository'

export const resolvers: Resolvers = {
    Query: {
        products: (root, args) => findAll(args),
        product: (root, { id }) => findById(id),
    },

    Mutation: {
        addProduct: (root, { product })  => {
            const productInput = {
                ...product,
                image: product.image.href
            }
            return addProduct(productInput)
        },

        removeProductById: (root, { id }) => removeProductById(id)
    }
}
