import { Resolvers } from '../../../types'
import { findByFilter } from '../repository'

export const resolvers: Resolvers = {
    Query: {
        searchProducts: (root, args) => {
            return findByFilter(args.skip, args.take, args.filter)
        },
    },
}
