import { Response } from 'express'
import multer from 'multer'
import { UPLOAD_DIR, FILE_FIELD, UPLOADS_ALIAS } from './constants'

const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, UPLOAD_DIR)
	},
	filename: function (req, file, cb) {
		const fileName = `${Date.now()}-${file.originalname}`
		cb(null, fileName)
	}
})

const multerInstance = multer({ storage: storage })
export const getMulterCb = () => multerInstance.single(FILE_FIELD)

export async function fileUploaderCb(req: Express.Request, res: Response) {
	const { file } = req

	if (file) {
		return res.send({
			url: `${UPLOADS_ALIAS}/${file.filename}`
		})
	} else {
		res.sendStatus(422)
	}
}