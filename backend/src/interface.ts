import { Request as ExpressRequest } from 'express'
import { Db } from 'mongodb'

export type Request = ExpressRequest

export interface Context {
    db: Db;
    req: Request;
}
