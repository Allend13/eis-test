# Start project

## Pre-deployed app
http://332623-allen28093.tmweb.ru:3000/

## Docker backend & frontend
You should have Docker installed and running.

Run:
* ``cd distribution/project-compose``
* ``docker-compose up``

Go to: http://localhost:3000

## Pre-deployed backend & front in development mode

Change `REACT_APP_SERVER_URL` in `frontend/.env` to `http://332623-allen28093.tmweb.ru:4000` 

Run:
* ``cd ../frontend``
* ``yarn install``
* ``yarn start``

Go to: http://localhost:3000